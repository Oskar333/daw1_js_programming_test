var elements = [
	{
		name: 'Banana',
		price: 200,
		qty: 31,
		imported: true
	},
	{
		name: 'Pomelo',
		price: 55,
		qty: 325,
		imported: false
	},
	{
		name: 'Piña',
		price: 70,
		qty: 125,
		imported: false
	},
	{
		name: 'Coco',
		price: 120,
		qty: 25,
		imported: true
	},
	{
		name: 'Papaya',
		price: 200,
		qty: 725,
		imported: true
	}
];
// 1.- Mostrar por consola todos los elementos formato objeto y formato array
// for(var i = 0; i < elements.length; i++) {
// 	console.log(elements[i]);
// }

// 2.- Mostrar por consola todos los elementos cuya cantidad sea menor de 50


// 3.- Mostrar por consola los elementos no importados


// 4.- Mostrar por consola los elementos cuyo precio sea mayor 60 y menor o igual de 120


// 5.- Mostrar por consola los elementos con precio 200 y cambiarlo a 230. 
//Mostrar nuevo array y el original
// REQUISITO: Creamos un nuevo array y guardamos todos los elementos ahí.
//No modificamos el array original
// Debemos crear nuevos objetos

// 6.- Sacar un listado por consola de este estilo 
//Banana 
//size: 12, 40, 60
//Pomelo 
//size: 5, 50, 55, 79